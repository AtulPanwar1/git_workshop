public class PrimitiveTypes
{
    public static void main(String args[])
    {
        byte b=120;
        short shortval=200;
        int intval=20000;
        long longval=45_00_000;
        char charval='A';
        int integerval=129;

        boolean flag=true;

        float floatval=45.49f;
        double doubleval=56999.34;
        
        byte reassignedIntValue=(byte) integerval;
        System.out.println(reassignedIntValue);
    }
}