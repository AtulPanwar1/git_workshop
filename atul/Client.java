public class Client
{
    public static void main(String args[])
    {
        BankAccount bnk=new saving();
        System.out.println("\n***Transaction done in saving account***");
        bnk.deposite(5000);
        bnk.deposite(3000);
        bnk.deposite(10);
        bnk.deposite(300);
        bnk.deposite(2000);
        System.out.println("Balance+="+bnk.getBalance());
        bnk.withdraw(1000);
        System.out.println("Balance="+bnk.getBalance());
        System.out.println(bnk);

        BankAccount bnk1=new Current();
        System.out.println("\n***Transaction done in Current account***");
        bnk1.deposite(5000);
        bnk1.deposite(1000);
        bnk1.deposite(100);
        bnk1.deposite(20);
        bnk1.deposite(300);
        bnk1.deposite(500);
        bnk1.deposite(1000);
        bnk1.deposite(100);
        System.out.println("Balance="+bnk1.getBalance());
        bnk1.withdraw(500);
        bnk1.withdraw(1000);
        bnk1.withdraw(2000);   
        bnk1.withdraw(10);
        System.out.println("Balance="+bnk1.getBalance());

        BankAccount bnk2=new Salary();
        System.out.println("\n***Transaction done in Salary account***");
        bnk2.deposite(5000);
        bnk2.deposite(1000);
        bnk2.deposite(20);
        bnk2.deposite(100);
        bnk2.deposite(200);
        bnk2.deposite(3000);
        System.out.println("Balance="+bnk2.getBalance());
        bnk2.withdraw(2000);
        bnk2.withdraw(100);
        bnk2.withdraw(2500);
        System.out.println("Balance="+bnk2.getBalance());
    }
}