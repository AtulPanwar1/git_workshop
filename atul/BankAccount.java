public abstract class BankAccount
{
   
 public abstract void deposite(int amount);

 public abstract void withdraw(int amount);

 public abstract int getBalance();
 
}