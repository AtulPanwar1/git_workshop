public class Salary extends BankAccount
{
    int balance=0;
    int amount=5000;

    @Override
    public void deposite(int amount)
    {
      balance=balance+amount;
    }
    @Override
    public void withdraw(int amount)
    {
        if((balance-amount)>=0)
        {
            balance=balance-amount;
        }
        else
        System.out.println("Cannot withdraw ..Low on balance");

    }
    @Override
    public int getBalance()
    {
        return balance;
    }
}