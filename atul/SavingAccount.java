public class SavingAccount
{
    private String name;
    private double acc_balance;
    private long acc_Id;
    private static int counter=1000;
    public SavingAccount(String name,double balance)
    {
        this.name=name;
        this.acc_balance=balance;
        this.acc_Id=counter++;
    }
    public SavingAccount(String name)
    {
        this.name=name;
        this.acc_Id=counter++;
    }
    public void deposit(double amount)
    {
        this.acc_balance+=amount;
    } 
    public void deposit(double amount,String ms)
    {
        this.acc_balance+=amount;
        System.out.println(ms)
    } 
    public double withdrawl(double amount)
    {
        if(this.acc_balance>amount)
        {
        this.acc_balance-=amount;
       
        }
        else
        System.out.println("Low on balance");
        return amount;
    }
    
    public double show_balance()
    {
        return acc_balance;
    }
    public String getName()
    {
        return this.name;
    }
    public long get_Id()
    {
        return this.acc_Id;
    }
}