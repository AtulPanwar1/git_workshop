public class CarClient
{
    public static void main(String args[])
    {
        int i=89;
        int [] array=new int[10];
        Car myCar=new Car();
        Car myNewCar=new Car();
        myCar.accelerate();
        myCar.accelerate();
        myCar.accelerate();
        myCar.accelerate();

        myNewCar.accelerate();
        myNewCar.accelerate();
        myNewCar.accelerate();

        System.out.println("The speed of myCar="+myCar.getCurrentSpeed());
        System.out.println("The speed of myNewCar="+myNewCar.getCurrentSpeed());
    }
}