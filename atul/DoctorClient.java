public class DoctorClient
{
    public static void main(String args[])
    {
        Doctor d1=new Doctor();

        Doctor d2=new Padietrician();

        Doctor d3=new OrthoPedician();

        //Compilation error
        // OrthoPedician d4=new Doctor();

        Doctor d5=d2;

       // OrthoPedician d6=d5//Error bec only referense is checked during compilation..
                              //though the object is of orthopedician
       // 
        //Padietrician d6=(Padietrician)d5;
        OrthoPedician d7=(OrthoPedician)d3;
      
       // d7.conductCTScan();
        Doctor doct=new OrthoPedician();
        doct.treatPatient();
        Doctor doct1=new Padietrician();
        doct1.treatPatient();
        Doctor doct2=new Doctor();
        doct2.treatPatient();
    }
}