public class AccountClient
{
    public static void main(String args[])
    {   
        SavingAccount client1=new SavingAccount("Atul",10000);
        SavingAccount client2=new SavingAccount("Gaurav");

        System.out.println("Account balance of "+client1.getName()+" having ID as "+client1.get_Id()+" is "+client1.show_balance());
        client1.withdrawl(4000);
        client1.deposit(2000,"First Salary");
        System.out.println("Account balance of "+client1.getName()+" having ID as "+client1.get_Id()+" is "+client1.show_balance());

        System.out.println("Account balance of "+client2.getName()+" having ID as "+client2.get_Id()+" is "+client2.show_balance());
        client2.deposit(2000);
        client2.deposit(8000,"First Salary");
        client2.withdrawl(4000);
        System.out.println("Account balance of "+client2.getName()+" having ID as "+client2.get_Id()+" is "+client2.show_balance());
   
        BankList list=new BankList();
        list.addBankAccount(client1);
        list.addBankAccount(client2);

    }
}