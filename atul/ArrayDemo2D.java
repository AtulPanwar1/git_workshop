public class ArrayDemo2D
{    
    static int row=4;
    static int col=4;
    static int initialValue=10;
    static int [][] intArray=new int[row][col];

    public static void main(String args[])
    {
        
        populateArray(intArray,initialValue);
        display2DArrayValues(intArray);

    }
    public static void populateArray(int[][]intArray,int initialValue)
    {
        for(int rowcount=0;rowcount<intArray.length;rowcount++)
        {
         for(int colcount=0;colcount<intArray.length;colcount++)
          {
           intArray[rowcount][colcount]=initialValue;
           initialValue++;
          }
        }
    }
    public static void display2DArrayValues(int[][]intArray)
    {
        for(int rowcount=0;rowcount<intArray.length;rowcount++)
        {
         for(int colcount=0;colcount<intArray.length;colcount++)
          {
           System.out.print(intArray[rowcount][colcount]+" ");
          }
          System.out.println();
        }

    }
 
    }
